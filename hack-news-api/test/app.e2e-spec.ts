import { HttpModule, HttpStatus, INestApplication } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { News, NewsDocument } from '../src/news/v1/entity/news.entity';
import { NewsModule } from '../src/news/v1/news.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const mockMongooseModel = {
    findOne: jest.fn().mockImplementation((dto) => {
      return dto;
    }),
    find: jest.fn().mockResolvedValue(Promise.resolve([{ story_id: 1234 }])),
    findOneAndUpdate: jest.fn().mockImplementation((filter, dto) => {
      return dto;
    }),
    create: jest.fn().mockResolvedValue((dto: NewsDocument) => {
      return dto;
    }),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [NewsModule, HttpModule],
    })
      .overrideProvider(getModelToken(News.name))
      .useValue(mockMongooseModel)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/v1/news/1 (GET)', () => {
    return request(app.getHttpServer())
      .get('/v1/news/1')
      .expect(HttpStatus.OK)
      .expect({ story_id: 1 });
  });

  it('/v1/news (GET)', () => {
    return request(app.getHttpServer())
      .get('/v1/news')
      .expect(HttpStatus.OK)
      .expect([{ story_id: 1234 }]);
  });

  it('/v1/news/1 (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/v1/news/1')
      .expect(HttpStatus.OK)
      .expect({ story_id: 1, deleted: true });
  });

  it('/v1/news (POST)', () => {
    const news = { story_id: 1 };
    return request(app.getHttpServer())
      .post('/v1/news')
      .set('Content-Type', 'application/json')
      .send(news)
      .expect(HttpStatus.CREATED)
      .expect(news);
  });
});
