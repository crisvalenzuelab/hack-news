import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'warn', 'log'],
  });

  // Configurando Cors
  app.enableCors({
    origin: 'http://localhost',
    methods: ['GET', 'POST', 'DELETE'],
  });

  // Configurando Contexto del proyecto
  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('Hack News Api')
    .setDescription('Api para el manejo de news')
    .setVersion('1.0.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/swagger-ui', app, document);
  await app.listen(3000);
}
bootstrap();
