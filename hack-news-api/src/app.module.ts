import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsClientModule } from './news-client/v1/news-client.module';
import { NewsModule } from './news/v1/news.module';

@Module({
  imports: [
    NewsModule,
    NewsClientModule,
    MongooseModule.forRoot('mongodb://reign:password@database:27017'),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
