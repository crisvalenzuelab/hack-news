import { HttpModule, HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsDto } from 'src/news/v1/dto/news.dto';
import { NewsService } from '../../../news/v1/service/news.service';
import { Hits } from '../classes/hits';
import { News } from '../classes/news';
import { NewsClientService } from './news-client.service';
import { AxiosResponse } from 'axios';
import { of } from 'rxjs';

describe('NewsClientService', () => {
  let service: NewsClientService;
  const response: AxiosResponse<Hits> = {
    data: { hits: [{ story_id: 1 } as News] } as Hits,
    status: 1,
    headers: '',
    statusText: 'ok',
    config: {},
  };

  beforeEach(async () => {
    const mockNewsService = {
      findById: jest
        .fn()
        .mockImplementation((id: number) => Promise.resolve({ story_id: id })),
      findAll: jest
        .fn()
        .mockImplementation(() => Promise.resolve([{ story_id: 1234 }])),
      delete: jest
        .fn()
        .mockImplementation((id: number) =>
          Promise.resolve({ story_id: id, deleted: true }),
        ),
      save: jest
        .fn()
        .mockImplementation((news: NewsDto) => Promise.resolve(news)),
    };

    const mockHttpService = {
      get: jest.fn().mockImplementation(() => of(response)),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsClientService, NewsService],
      imports: [HttpModule],
    })
      .overrideProvider(NewsService)
      .useValue(mockNewsService)
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile();

    service = module.get<NewsClientService>(NewsClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('deberia retornar las noticias a guardar', () => {
    expect(service.retriveData()).resolves.toEqual(response);
  });
});
