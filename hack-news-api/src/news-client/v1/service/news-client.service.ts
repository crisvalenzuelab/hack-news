import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { NewsService } from '../../../news/v1/service/news.service';
import { Hits } from '../classes/hits';
import { News } from '../classes/news';

@Injectable()
export class NewsClientService {
  constructor(
    private readonly httpService: HttpService,
    private readonly newsService: NewsService,
  ) {}

  async retriveData(): Promise<AxiosResponse<Hits>> {
    const params = new URLSearchParams({ query: 'nodejs' });
    const response: AxiosResponse<Hits> = await this.httpService
      .get<Hits>('https://hn.algolia.com/api/v1/search_by_date', {
        params: params,
      })
      .toPromise();
    this.saveData(response.data.hits);
    return response;
  }

  async saveData(newsList: News[]) {
    for (const news of newsList) {
      if (news.story_id) await this.newsService.save(news);
    }
  }
}
