import { Controller, Get, HttpCode, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';
import { Hits } from '../classes/hits';
import { News } from '../classes/news';
import { NewsClientService } from '../service/news-client.service';

@Controller('v1/news-client')
export class NewsClientController {
  constructor(private newsClientService: NewsClientService) {}

  @Get('/load')
  @HttpCode(HttpStatus.CREATED)
  async load(@Res() response: Response) {
    const result: Hits = (await this.newsClientService.retriveData()).data;
    const news: News[] = result.hits;

    if (!news && news.length === 0)
      return response.status(HttpStatus.NO_CONTENT).send();

    return response.status(HttpStatus.OK).send(news);
  }
}
