import { Test, TestingModule } from '@nestjs/testing';
import { NewsClientService } from '../service/news-client.service';
import { NewsClientController } from './news-client.controller';

describe('NewsClientController', () => {
  let controller: NewsClientController;

  beforeEach(async () => {
    const mockNewsClientService = {};

    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsClientController],
      providers: [NewsClientService],
    })
      .overrideProvider(NewsClientService)
      .useValue(mockNewsClientService)
      .compile();

    controller = module.get<NewsClientController>(NewsClientController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  //TODO probar carga de datos
});
