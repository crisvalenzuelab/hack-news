import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { News, NewsSchema } from 'src/news/v1/entity/news.entity';
import { NewsModule } from 'src/news/v1/news.module';
import { NewsService } from 'src/news/v1/service/news.service';
import { NewsClientController } from './controller/news-client.controller';
import { NewsClientService } from './service/news-client.service';

@Module({
  imports: [
    NewsModule,
    HttpModule.register({ timeout: 5000 }),
    MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
  ],
  controllers: [NewsClientController],
  providers: [NewsService, NewsClientService],
})
export class NewsClientModule {}
