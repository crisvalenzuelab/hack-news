import { News } from './news';

const news = new News({
  objectID: '26957546',
  created_at: new Date('2021-04-27T15:47:51.000Z'),
  title: null,
  url: null,
  author: 'juancn',
  points: null,
  story_text: null,
  comment_text: 'There are several components I&#x27;m responsible for.<p>',
  num_comments: null,
  story_id: 26935504,
  story_title: 'A Product Engineer’s Guide to Platform Engineers (2019)',
  story_url:
    'https://rinaarts.medium.com/stupid-baboons-stubborn-elephants-c33412541bb1',
  parent_id: 26939912,
  created_at_i: 1619538471,
  deleted: false,
});

describe('News', () => {
  it('should be defined', () => {
    expect(new News()).toBeDefined();
  });
});

it('should get attribute values of news', () => {
  expect(new News(news).objectID).toBe(news.objectID);
  expect(new News(news).created_at).toBe(news.created_at);
  expect(new News(news).title).toBe(news.title);
  expect(new News(news).url).toBe(news.url);
  expect(new News(news).author).toBe(news.author);
  expect(new News(news).story_text).toBe(news.story_text);
  expect(new News(news).comment_text).toBe(news.comment_text);
  expect(new News(news).num_comments).toBe(news.num_comments);
  expect(new News(news).story_id).toBe(news.story_id);
  expect(new News(news).story_title).toBe(news.story_title);
  expect(new News(news).story_url).toBe(news.story_url);
  expect(new News(news).parent_id).toBe(news.parent_id);
  expect(new News(news).created_at_i).toBe(news.created_at_i);
  expect(new News(news).deleted).toBe(news.deleted);
});
