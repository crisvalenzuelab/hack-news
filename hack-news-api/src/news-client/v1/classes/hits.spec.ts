import { Hits } from './hits';

describe('Hits', () => {
  const hits = new Hits({
    hits: [
      {
        objectID: '26957546',
        created_at: new Date('2021-04-27T15:47:51.000Z'),
        title: null,
        url: null,
        author: 'juancn',
        points: null,
        story_text: null,
        comment_text:
          'There are several components I&#x27;m responsible for.<p>',
        num_comments: null,
        story_id: 26935504,
        story_title: 'A Product Engineer’s Guide to Platform Engineers (2019)',
        story_url:
          'https://rinaarts.medium.com/stupid-baboons-stubborn-elephants-c33412541bb1',
        parent_id: 26939912,
        created_at_i: 1619538471,
        deleted: false,
      },
    ],
  });

  it('should be defined', () => {
    expect(new Hits()).toBeDefined();
  });

  it('accesadores', () => {
    expect(new Hits(hits).hits[0].author).toBe(hits.hits[0].author);
    expect(new Hits(hits).hits[0].objectID).toBe(hits.hits[0].objectID);
    expect(new Hits(hits).hits[0].created_at).toBe(hits.hits[0].created_at);
    expect(new Hits(hits).hits[0].comment_text).toBe(hits.hits[0].comment_text);
    expect(new Hits(hits).hits[0].story_title).toBe(hits.hits[0].story_title);
    expect(new Hits(hits).hits[0].story_url).toBe(hits.hits[0].story_url);
    expect(new Hits(hits).hits[0].parent_id).toBe(hits.hits[0].parent_id);
    expect(new Hits(hits).hits[0].created_at_i).toBe(hits.hits[0].created_at_i);
    expect(new Hits(hits).hits[0].deleted).toBe(hits.hits[0].deleted);
  });
});
