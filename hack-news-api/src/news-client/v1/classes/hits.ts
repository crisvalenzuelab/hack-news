import { News } from './news';

export class Hits {
  hits: News[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  query: string;
  params: string;
  processingTimeMS: number;

  constructor(hits?: Partial<Hits>) {
    Object.assign(this, hits);
  }
}
