import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Logger,
  NotAcceptableException,
  Param,
  ParseIntPipe,
  Post,
  UsePipes
} from '@nestjs/common';
import { NewsDto } from '../../dto/news.dto';
import { NewsService } from '../../service/news.service';

@Controller('v1/news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get('/')
  async getAll(): Promise<NewsDto[]> {
    return this.newsService.findAll();
  }

  @Get('/:id')
  @UsePipes(ParseIntPipe)
  async getById(@Param('id') id: number): Promise<NewsDto> {
    Logger.debug(`[NewsController][getById] id de busqueda ${id}`);
    return await this.newsService.findById(id);
  }

  @Post('/')
  @HttpCode(HttpStatus.CREATED)
  @Header('Content-Type', 'application/json')
  async create(@Body() news: NewsDto): Promise<NewsDto> {
    Logger.debug(
      `[NewsController][create] objeto a crear ${JSON.stringify(news)}`,
    );
    try {
      const result = await this.newsService.save(news);
      if (!result) throw new NotAcceptableException();
      return result;
    } catch (err) {
      if (err)
        throw new HttpException(
          { messaage: err.message },
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  @UsePipes(ParseIntPipe)
  async delete(@Param('id') id: number): Promise<NewsDto> {
    try {
      Logger.debug(`[NewsController][delete] noticia a eliminar ${id}`);
      return this.newsService.delete(id);
    } catch (err) {
      if (err)
        throw new HttpException(
          { messaage: err.message },
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
  }
}
