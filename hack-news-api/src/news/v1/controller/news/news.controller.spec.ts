import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsDto } from '../../dto/news.dto';
import { NewsService } from '../../service/news.service';
import { NewsController } from './news.controller';

describe('NewsController', () => {
  let controller: NewsController;
  let app: INestApplication;

  beforeEach(async () => {
    const mockNewsService = {
      findById: jest
        .fn()
        .mockImplementation((id: number) => Promise.resolve({ story_id: id })),
      findAll: jest
        .fn()
        .mockImplementation(() => Promise.resolve([{ story_id: 1234 }])),
      delete: jest
        .fn()
        .mockImplementation((id: number) =>
          Promise.resolve({ story_id: id, deleted: true }),
        ),
      save: jest
        .fn()
        .mockImplementation((news: NewsDto) => Promise.resolve(news)),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(mockNewsService)
      .compile();

    app = module.createNestApplication();
    controller = module.get<NewsController>(NewsController);
    await app.init();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should get a news by story id', () => {
    expect(controller.getById(1)).resolves.toEqual({ story_id: 1 });
  });

  it('should get an array of news', () => {
    expect(controller.getAll()).resolves.toEqual([{ story_id: 1234 }]);
  });

  it('should delete news', () => {
    expect(controller.delete(1)).resolves.toEqual({
      story_id: 1,
      deleted: true,
    });
  });

  it('should save a news', () => {
    expect(controller.create({ story_id: 1 } as NewsDto)).resolves.toEqual({
      story_id: 1,
    });
  });
});
