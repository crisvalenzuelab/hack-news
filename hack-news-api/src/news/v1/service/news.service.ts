import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News, NewsDocument } from '../entity/news.entity';

@Injectable()
export class NewsService {
  private readonly newsModel: Model<NewsDocument>;

  constructor(@InjectModel(News.name) newsModel: Model<NewsDocument>) {
    this.newsModel = newsModel;
  }

  async save(news: News): Promise<News> {
    const newsdto = await this.findById(news.story_id);
    Logger.debug(`[NewsService][save] noticia encontrada ${!newsdto}`);

    if (!newsdto) {
      Logger.debug('[NewsService][save] creando noticia');
      return await this.newsModel.create(news);
    } else {
      Logger.debug('[NewsService][save] actualizando noticia');
      await this.update(news);
      return newsdto;
    }
  }

  async findById(id: number): Promise<News> {
    Logger.debug(`[NewsService][findById] id de busqueda ${id}`);
    return this.newsModel.findOne(
      { story_id: id },
      (err: Error, response: NewsDocument) => {
        if (err) Promise.reject(err);
        Promise.resolve(response);
      },
    );
  }

  async findAll(): Promise<News[]> {
    return await this.newsModel.find((err: Error, response: NewsDocument[]) => {
      if (err) Promise.reject(err);
      Promise.resolve(response);
    });
  }

  async delete(id: number): Promise<NewsDocument> {
    Logger.debug(`[NewsService][delete] id a eliminar ${id}`);
    const news: News = await this.findById(id);
    if (news) {
      news.deleted = true;
    }

    return await this.update(news);
  }

  async update(news: News): Promise<NewsDocument> {
    return this.newsModel.findOneAndUpdate({story_id: news.story_id},news, {runValidators: true});
  }
}
