import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { News, NewsDocument } from '../entity/news.entity';
import { NewsService } from './news.service';

describe('NewsService', () => {
  let service: NewsService;

  const mockMongooseModel = {
    findOne: jest.fn().mockImplementation((dto) => {
      return dto;
    }),
    find: jest.fn().mockResolvedValue(Promise.resolve([{ story_id: 1234 }])),
    findOneAndUpdate: jest.fn().mockImplementation((filter, dto) => {
      return dto;
    }),
    create: jest.fn().mockResolvedValue((dto: NewsDocument) => {
      return dto;
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        { provide: getModelToken(News.name), useValue: mockMongooseModel },
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return news by id', async () => {
    expect(service.findById(1)).resolves.toEqual({ story_id: 1 });
  });

  it('should return an array of news', async () => {
    expect(service.findAll()).resolves.toEqual([{ story_id: 1234 }]);
  });

  it('should update a news', async () => {
    expect(service.update({ story_id: 2 } as News)).resolves.toEqual({
      story_id: 2,
    });
  });

  it('should delete a news', async () => {
    expect(service.delete(2)).resolves.toEqual({ story_id: 2, deleted: true });
  });

  it('should save a news', async () => {
    expect(service.save({ story_id: 1 } as News)).resolves.toEqual({
      story_id: 1,
    });
  });
});
