import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema()
export class News {
  @Prop({ required: true })
  objectID: string;
  @Prop({ required: true })
  parent_id: number;
  @Prop({ required: true })
  created_at_i: number;
  @Prop({ required: true, unique: true })
  story_id: number;
  @Prop()
  created_at: Date;
  @Prop()
  title: string;
  @Prop()
  url: string;
  @Prop()
  author: string;
  @Prop()
  points: string;
  @Prop()
  story_text: string;
  @Prop()
  comment_text: string;
  @Prop()
  num_comments: number;
  @Prop()
  story_title: string;
  @Prop()
  story_url: string;
  @Prop({ default: false })
  deleted: boolean;

  // constructor(news?: Partial<News>) {
  //     Object.assign(this, news);
  // }
}

export const NewsSchema = SchemaFactory.createForClass(News);
