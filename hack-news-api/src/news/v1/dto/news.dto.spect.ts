import { NewsDto } from './news.dto';

const news = new NewsDto({
  objectID: '26957546',
  created_at: new Date('2021-04-27T15:47:51.000Z'),
  title: null,
  url: null,
  author: 'juancn',
  points: null,
  story_text: null,
  comment_text: 'There are several components I&#x27;m responsible for.<p>',
  num_comments: null,
  story_id: 26935504,
  story_title: 'A Product Engineer’s Guide to Platform Engineers (2019)',
  story_url:
    'https://rinaarts.medium.com/stupid-baboons-stubborn-elephants-c33412541bb1',
  parent_id: 26939912,
  created_at_i: 1619538471,
  deleted: false,
});

describe('NewsDto', () => {
  it('should be defined', () => {
    expect(new NewsDto()).toBeDefined();
  });
});

it('should get attribute values of news', () => {
  expect(new NewsDto(news).objectID).toBe(news.objectID);
  expect(new NewsDto(news).created_at).toBe(news.created_at);
  expect(new NewsDto(news).title).toBe(news.title);
  expect(new NewsDto(news).url).toBe(news.url);
  expect(new NewsDto(news).author).toBe(news.author);
  expect(new NewsDto(news).story_text).toBe(news.story_text);
  expect(new NewsDto(news).comment_text).toBe(news.comment_text);
  expect(new NewsDto(news).num_comments).toBe(news.num_comments);
  expect(new NewsDto(news).story_id).toBe(news.story_id);
  expect(new NewsDto(news).story_title).toBe(news.story_title);
  expect(new NewsDto(news).story_url).toBe(news.story_url);
  expect(new NewsDto(news).parent_id).toBe(news.parent_id);
  expect(new NewsDto(news).created_at_i).toBe(news.created_at_i);
  expect(new NewsDto(news).deleted).toBe(news.deleted);
});
