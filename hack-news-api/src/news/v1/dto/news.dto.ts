import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class NewsDto {
  
  @IsString()
  @IsOptional()
  objectID: string;

  @IsNumber()
  @Min(0)
  @IsOptional()
  parent_id: number;

  @IsNumber()
  @Min(0)
  @IsOptional()
  created_at_i: number;

  @IsNumber()
  @Min(0)
  @IsOptional()
  @ApiProperty({ type: Number, description: 'Id de la historia' })
  story_id: number;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: Date, description: 'Fecha de creación' })
  created_at: Date;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Título' })
  title: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'URL' })
  url: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Autor' })
  author: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Points' })
  points: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Cuerpo de la historia' })
  story_text: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Comentario' })
  comment_text: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: Number, description: 'Cantidad de comentarios' })
  num_comments: number;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'Titulo de la historia' })
  story_title: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ type: String, description: 'URL de la historia' })
  story_url: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    type: Boolean,
    description: 'True si la historia ha sido eliminada',
    default: false,
  })
  deleted = false;

  constructor(news?: Partial<NewsDto>) {
    Object.assign(this, news);
  }
}
