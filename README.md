# Hack News
This application is intended to demonstrate technical capabilities for the Reign selection process, according to the sent by the interviewer.

To access the web application must navigate to [Hack News!](http://localhost/home)

## App Requirements
* Nodejs (with npm)
* Docker
* Docker-compose
* Angular cli (latest version)
* Nestjs cli (latest version)

## Start APP

To start stack with Docker Compose:
```
docker-compose up -d
```

To check logs run:
```
docker-compose logs
```

## Load Data

To load data to database open the  [URL](http://localhost:3000/api/v1/news-client/load) on browser

