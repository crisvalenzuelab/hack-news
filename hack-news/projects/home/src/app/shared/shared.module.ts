import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {DateHourFilterPipe} from './pipe/date-hour-filter.pipe';
import {NotNullStringPipe} from './pipe/not-null-string.pipe';
import {SortByPipe} from './pipe/sort-by.pipe';

@NgModule({
    declarations: [DateHourFilterPipe, NotNullStringPipe, SortByPipe],
    imports: [CommonModule, RouterModule],
    exports: [CommonModule, RouterModule, DateHourFilterPipe, NotNullStringPipe, SortByPipe]
})
export class SharedModule {
}
