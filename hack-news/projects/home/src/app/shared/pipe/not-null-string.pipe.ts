import {Pipe, PipeTransform} from '@angular/core';
import {News} from '../../features/news/models/news';

@Pipe({
    name: 'notNullString'
})
export class NotNullStringPipe implements PipeTransform {

    transform(news: any, ...args: string[]): string {
        for (const key of args) {
            const value = news[key as keyof News];
            if (value) {
                return value.toString();
            }
        }

        return '-';
    }
}
