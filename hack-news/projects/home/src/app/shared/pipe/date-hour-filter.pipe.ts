import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import {formatDate} from '@angular/common';
import * as moment from 'moment/moment';

@Pipe({
    name: 'dateHourFilter'
})
export class DateHourFilterPipe implements PipeTransform {
    transform(value: Date, format: string): string {
        if (value) {
            const today = moment();
            const yesterday = moment().subtract(1 , 'day');
            if (today.isSame(value, 'day')) {
                return formatDate(value, 'shortTime', moment().locale());
            } else if (yesterday.isSame(value, 'day')) {
                return 'yesterday';
            } else if (yesterday.isAfter(value, 'day')) {
                return formatDate(value, 'MMM d', moment().locale());
            }
            return formatDate(value, format, moment().locale());
        }

        throw Error('No es una fecha valida');
    }

    constructor(@Inject(LOCALE_ID) private locale: Location) {
    }
}
