import {Component, OnInit} from '@angular/core';
import {NewsClientService} from './services/news-client.service';
import {News} from './models/news';
import * as _ from 'lodash';
import {NotNullStringPipe} from '../../shared/pipe/not-null-string.pipe';

@Component({
    selector: 'hack-news-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
    newsList: News[] = [];


    constructor(private newsClientService: NewsClientService) {
    }

    ngOnInit(): void {
        this.newsClientService.getAll().subscribe(result => {
            this.newsList = result;
        });
    }

    delete(news: News, event: Event): void {
        event.stopPropagation();
        this.newsClientService.delete(news.story_id).subscribe((response) => {
            if (response.status === 200) {
                news.deleted = true;
            }
        });
    }

    openUrl(news: News): void {
        const url = new NotNullStringPipe().transform(news, 'story_url', 'story');
        if (url !== '-') {
            window.open(news.story_url, '_blank');
        }
    }
}
