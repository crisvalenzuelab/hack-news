import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {News} from '../models/news';

@Injectable()
export class NewsClientService {

    private BASE_URL = 'http://localhost:3000/api/v1';

    constructor(private http: HttpClient) {
    }

    getAll(): Observable<News[]> {
        const API_URL = `${this.BASE_URL}/news`;
        return this.http.get<News[]>(API_URL);
    }

    delete(id: number): Observable<any> {
        const API_URL = `${this.BASE_URL}/news/${id}`;
        return this.http.delete(API_URL, {observe: 'response'});
    }
}
