import { TestBed } from '@angular/core/testing';

import { NewsClientService } from './news-client.service';

describe('NewsClientService', () => {
  let service: NewsClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewsClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
