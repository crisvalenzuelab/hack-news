import {NgModule} from '@angular/core';

import {NewsRoutingModule} from './news-routing.module';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
    declarations: [],
    providers: [],
    imports: [NewsRoutingModule, HttpClientModule],
    exports: []
})
export class NewsModule {
}
