export interface News {
    objectID: string;
    parent_id: number;
    created_at_i: number;
    story_id: number;
    created_at: Date;
    title: string;
    url: string;
    author: string;
    points: string;
    story_text: string;
    comment_text: string;
    num_comments: number;
    story_title: string;
    story_url: string;
    deleted: boolean;
}
