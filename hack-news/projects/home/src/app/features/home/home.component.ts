import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hack-news-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title  = 'HN Feed';
  subtitle = ' We <3 hacker news!';

  constructor() { }

  ngOnInit(): void {
  }

}
