import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { NewsComponent } from '../news/news.component';
import { NewsClientService } from '../news/services/news-client.service';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';



@NgModule({
    declarations: [HomeComponent, NewsComponent],
    providers: [NewsClientService],
    imports: [CommonModule, HomeRoutingModule, HttpClientModule, SharedModule]
})
export class HomeModule {
}
