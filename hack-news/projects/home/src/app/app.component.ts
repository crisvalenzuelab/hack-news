import { Component } from '@angular/core';

@Component({
  selector: 'hack-news-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'HN Feed!';
}
